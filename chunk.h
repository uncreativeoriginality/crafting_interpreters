#ifndef clox_chunk_h
#define clox_chunk_h

#include "common.h"
#include "value.h"

// bytecode operation codes
typedef enum {
	OP_CONSTANT,
	OP_RETURN,
} OpCode;

// change dynamic array to heap?
typedef struct {
	int count;	// elements in use
	int capacity;	// number of elements in the array
	uint8_t* code;	// dynamic array
	int* lines;
	ValueArray constants;
} Chunk;

// TODO: add proper doxygen comments

void initChunk(Chunk* chunk);

// free chunk
void freeChunk(Chunk* chunk);

// appends byte to end of chunk
void writeChunk(Chunk* chunk, uint8_t byte, int line);

int addConstant(Chunk* chunk, Value value);

#endif
