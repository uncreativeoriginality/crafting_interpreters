default: main

main: main.c memory.o chunk.o debug.o value.o
	$(CC) -o main memory.o chunk.o debug.o value.o main.c

clean:
	rm *.o main
test: clean main
	./main
